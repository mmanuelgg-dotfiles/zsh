ZSH_CUSTOM=$HOME/.config/zsh
ZSH_LOCAL=$HOME/.local/share/zsh

if [[ -f "/opt/homebrew/bin/brew" ]] then
  eval "$(/opt/homebrew/bin/brew shellenv)"
fi

# Set the directory we want to store zinit and plugins
ZINIT_HOME="${XDG_DATA_HOME:-${HOME}/.local/share}/zinit/zinit.git"

# Download Zinit, if it's not there yet
if [ ! -d "$ZINIT_HOME" ]; then
   mkdir -p "$(dirname $ZINIT_HOME)"
   git clone https://github.com/zdharma-continuum/zinit.git "$ZINIT_HOME"
fi

# Source/Load zinit
source "${ZINIT_HOME}/zinit.zsh"

# Add zsh plugins
zinit light-mode for \
  zsh-users/zsh-completions \
  zsh-users/zsh-autosuggestions \
  zsh-users/zsh-syntax-highlighting \
  Aloxaf/fzf-tab \
  Freed-Wu/fzf-tab-source \
  atuinsh/atuin

# Add snippets
zinit snippet OMZP::git
zinit snippet OMZP::sudo
zinit snippet OMZP::archlinux
zinit snippet OMZP::aws
# zinit snippet OMZP::kubectl
zinit snippet OMZP::kubectx
zinit snippet OMZP::command-not-found

if type brew &>/dev/null; then
  FPATH="$(brew --prefix)/share/zsh/site-functions:${FPATH}"
fi

# Load completions
autoload -Uz compinit && compinit

zinit cdreplay -q

# Keybindings
bindkey -e
bindkey '^p' history-search-backward
bindkey '^n' history-search-forward
bindkey '^[w' kill-region


# History
HISTSIZE=5000
HISTFILE=~/.zsh_history
SAVEHIST=$HISTSIZE
HISTDUP=erase
setopt appendhistory
setopt sharehistory
setopt hist_ignore_space
setopt hist_ignore_all_dups
setopt hist_save_no_dups
setopt hist_ignore_dups
setopt hist_find_no_dups

# Completion styling
zstyle ':completion:*' completer _expand _extensions _complete _ignored _correct _approximate
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}' 'm:{a-zA-Z}={A-Za-z} l:|=* r:|=*' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*' 
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
zstyle ':completion:*' file-list all # ls -l style
zstyle ':completion:*' menu no
zstyle ':completion:*:descriptions' format '[%d]'
zstyle ':completion:*:corrections' format '%F{yellow}-- %d (errors: %e) --%f'
zstyle ':completion:*:messages' format ' %F{purple} -- %d --%f'
zstyle ':completion:*:warnings' format ' %F{red}-- no matches found --%f'
zstyle ':fzf-tab:*' switch-group '<' '>'
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'ls --color $realpath'
zstyle ':fzf-tab:complete:__zoxide_z:*' fzf-preview 'ls --color $realpath'

command -v lesspipe.sh &> /dev/null && eval "$(lesspipe.sh)"


# PATH
paths=( \
  "$HOME/.bin" \
  "$HOME/.config/emacs/bin" \
  "$HOME/.local/bin" \
  "$HOME/.cargo/bin" \
  "$HOME/.local/share/cargo/bin" \
  "$HOME/.local/share/npm/bin" \
  "$HOME/.ghcup/bin" \
  "$HOME/Library/Application Support/Coursier/bin"
)
for dir in $paths; do
  [[ -d "$dir" ]] && export PATH="$dir:$PATH"
done

# ENV
export EDITOR="nvim"
export BROWSER="firefox"
[[ -f $HOME/.kube/config ]] && export KUBECONFIG=$HOME/.kube/config
# [[ -f $HOME/.google/credentials/cloud-key.json ]] && export GOOGLE_APPLICATION_CREDENTIALS="$HOME/.google/credentials/cloud-key.json"
export JAVA_OPTS="-Djava.awt.headless=true -Dawt.toolkit=sun.awt.HToolkit"
## MacOS
command -v brew &> /dev/null && export DYLD_LIBRARY_PATH="$(brew --prefix)/lib:$DYLD_LIBRARY_PATH"

# Source
[[ -f "$HOME/.ghcup/env" ]] && source "$HOME/.ghcup/env" # ghcup-env
[[ -s "/etc/grc.zsh" ]] && source /etc/grc.zsh # If grc is installed, it will colorize general commands such as `ping`


if [ -d "$HOME/.nvm" ]; then
  export NVM_DIR="$HOME/.nvm"
  [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
  [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
fi

# Aliases
alias _zsh_config_check_dependencies="$HOME/.config/zsh/check_dependencies.sh"

alias cd..="cd .."
alias ..="cd .."
alias ...="cd ../.."

if command -v fd &> /dev/null; then
  alias sd="cd ~  && cd \$(fd -t d | fzf)"
else
  alias sd="cd ~  && cd \$(find * -type d | fzf)"
fi

## ls
alias ls="ls --color=auto"
if command -v eza &> /dev/null; then
  alias ls="eza --icons --group-directories-first"
  alias ll="eza -al --icons --git --group-directories-first"
  alias lls="eza -al --icons --git --group-directories-first --total-size"
  alias la="eza -a --icons --group-directories-first"
  alias lt="eza -l --tree --level=2  --icons --git --group-directories-first"
  alias lts="eza -l --tree --level=2  --icons --git --total-size --group-directories-first"
  alias lta="eza -al --tree --level=2 --icons --git --group-directories-first"
  alias ltt="eza -l --tree --icons --git --group-directories-first"
  alias ltta="eza -al --tree --icons --git --group-directories-first"
fi

## Continue download
alias wget "wget -c"

## JAVA
alias jvms='/usr/libexec/java_home -V'
usejava() {
   export JAVA_HOME=`/usr/libexec/java_home -v $1`
}

## Linux
### Arch
alias pacman="pacman --color auto"

### Grub
alias grub-update="sudo grub-mkconfig -o /boot/grub/grub.cfg"
#### grub issue 08/2022
alias install-grub-efi="sudo grub-install --target=x86_64-efi --efi-directory=/boot/efi"

### Wayland aliases
alias wsimplescreen="wf-recorder -a"
alias wsimplescreenrecorder="wf-recorder -a -c h264_vaapi -C aac -d /dev/dri/renderD128 --file=recording.mp4"


## MacOS
### Brew Helm
if [ -d "/usr/local/Cellar/helm@2" ]; then
  alias helm2="/usr/local/Cellar/helm@2/2.17.0/bin/helm"
fi
command -v gfind &> /dev/null && alias find="gfind"
export BROWSER="/Applications/Firefox.app/Contents/MacOS/firefox"
### Nix
if [ -d /nix ] && ! command -v nix &> /dev/null; then
  NIX_CONFIG_BLOCK=$(cat <<'EOF'
# Nix
if [ -e '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh' ]; then
  if [ ! -x '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh' ]; then
    sudo chmod +x '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh'
  fi
  source '/nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh'
fi
# End Nix
EOF
)
  if ! grep -q "# Nix" /etc/zshrc; then
    echo "$NIX_CONFIG_BLOCK" | sudo tee -a /etc/zshrc > /dev/null
    echo "Added Nix configuration block to /etc/zshrc."
  fi
fi

### Nix home-manager (if it does not manage the shell itself)
[[ -f "$HOME/.nix-profile/etc/profile.d/hm-session-vars.sh" ]] && source "$HOME/.nix-profile/etc/profile.d/hm-session-vars.sh"


# Functions

## ex = EXtractor for all kinds of archives
## usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   tar xf $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

show_colors() {
  for i in {0..255}; do
    printf "\x1b[48;5;${i}mcolour${i}\x1b[0m \x1b[38;5;${i}mcolour${i} \n"
  done
}

show_colors_16() {
  for i in {0..15}; do
    printf "\x1b[48;5;${i}mcolour${i}\x1b[0m \x1b[38;5;${i}mcolour${i} \n"
  done
}

# Shell integrations
command -v fzf &> /dev/null && eval "$(fzf --zsh)"
command -v zoxide &> /dev/null && eval "$(zoxide init --cmd cd zsh)"

# Prompt
if command -v oh-my-posh &> /dev/null; then
  eval "$(oh-my-posh init zsh --config $HOME/.config/ohmyposh/zen.toml)"
elif command -v starship &> /dev/null; then
  eval "$(starship init zsh)"
else
  local prompt_symbol="%B%F{green}❯%f%b "
  local host_name="%B%F{blue}%m%f%b"
  local user_name="%B%F{green}%n%f%b"
  local dir_name="%B%F{cyan}%~%f%b"

  setopt PROMPT_SUBST
  PROMPT='${user_name}@${host_name}:${dir_name} ${vcs_info_msg_0_}'$'\n''${prompt_symbol}'
  RPROMPT="%F{red}%(?.%F{green}✔%F{red}.✘)%f %B%F{yellow}%D{%H:%M:%S}%f%b"
fi

# Shell initialization
command -v fastfetch &> /dev/null && fastfetch

## MacOS
# test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh" || true
