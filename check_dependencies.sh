#!/bin/zsh

echo "Checking dependencies..."

echo "Base zsh:"

command -v zoxide &>/dev/null && echo "-> zoxide: installed" || echo "-> zoxide: not installed"
command -v eza &>/dev/null && echo "-> eza: installed" || echo "-> eza: not installed"
command -v lsd &>/dev/null && echo "-> lsd: installed" || echo "-> lsd: not installed"
command -v bat &>/dev/null && echo "-> bat: installed" || echo "-> bat: not installed"
command -v fd &>/dev/null && echo "-> fd: installed" || echo "-> fd: not installed"
command -v rg &>/dev/null && echo "-> rg: installed" || echo "-> rg: not installed"

echo "fzf-tab dependencies:"

command -v fzf &>/dev/null && echo "-> fzf: installed" || echo "-> fzf: not installed. Main dependency"
command -v chafa &>/dev/null && echo "-> chafa: installed" || echo "-> chafa: not installed. For image preview"
command -v pdftotext &>/dev/null && echo "-> pdftotext: installed" || echo "-> pdftotext: not installed. For pdf preview"
command -v hr &>/dev/null && echo "-> hr: installed" || echo "-> hr: not installed. For horizontal rule in pdf preview"
command -v emojify &>/dev/null && echo "-> emojify: installed $(emojify :white_check_mark:)" || echo "-> emojify: not installed. For emoji support"
command -v jq &>/dev/null && echo "-> jq: installed" || echo "-> jq: not installed. For json parsing"
command -v jupyter &>/dev/null && echo "-> jupyter: installed" || echo "-> jupyter: not installed. For ipynb preview"
